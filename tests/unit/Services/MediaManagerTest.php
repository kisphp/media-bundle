<?php

namespace Kisphp\MediaBundle\Unit\Services;

use Codeception\Test\Unit;
use Kisphp\MediaBundle\Services\MediaManager;
use Kisphp\Utils\Files;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

/**
 * @group up
 */
class MediaManagerTest extends Unit
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $sc;

    protected function _create_demo_picture()
    {
        $img = imagecreate(600, 600);
        imagecolorallocate( $img, 0, 0, 255 );

        $root = realpath(__DIR__ . '/../../../../');
        $params = Yaml::parse(file_get_contents($root . '/app/config/parameters.yml'));

        $target = Files::inc($params['parameters']['upload_directory'] . '/test');

        if (is_dir($target) === false) {
            mkdir($target, 0755, true);
        }
        if (is_dir($target . '/../images/') === false) {
            mkdir($target . '/../images/', 0755, true);
        }

        copy($root . '/tests/_data/no-image-available.png', $target . '/../images/no-image-available.png');

        imagepng($img, codecept_data_dir('no-image-available.png'));
    }

    /**
     * @group upload
     */
    public function testUpload()
    {
        $this->assertTrue(true);
//        $mm = new MediaManager($this->sc);
//
//        $request = Request::createFromGlobals();
//        $file = new UploadedFile(
//            codecept_data_dir('no-image-available.png'),
//            'test-image',
//            null,
//            null,
//            null,
//            true
//        );
//
//        $request->files->set('file', $file);
//
//        $content = json_decode($mm->uploadImages($request, 'article', 1)->getContent(), true);
//
//        $this->assertArrayHasKey('code', $content);
//        $this->assertArrayHasKey('id', $content);
    }
}
