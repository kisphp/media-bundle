<?php

namespace Functional\MediaBundle\Controller;

use Kisphp\Utils\Files;
use Symfony\Component\Yaml\Yaml;

/**
 * @group thumb
 */
class ThumbControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function existed_thumbnail(\FunctionalTester $i)
    {
        $this->_create_demo_file();

        $i->amOnPage('/thumbs/zzz/200x200_test-image.jpg');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function existed_thumbnail_not_found_should_not_have_an_error(\FunctionalTester $i)
    {
        $i->amOnPage('/thumbs/zzz/200x200_test-image-not-found.jpg');
        $i->canSeeResponseCodeIs(200);
    }

    protected function _create_demo_file()
    {
        $img = imagecreate(600, 600);
        imagecolorallocate( $img, 0, 0, 255 );

        $root = realpath(__DIR__ . '/../../../../../../../');

        $params = Yaml::parse(file_get_contents($root . '/app/config/parameters.yml'));

        $target = Files::inc($params['parameters']['upload_directory'] . '/test');

        if (is_dir($target) === false) {
            mkdir($target, 0755, true);
        }
        if (is_dir($target . '/../images/') === false) {
            mkdir($target . '/../images/', 0755, true);
        }

        copy($root . '/tests/_data/no-image-available.png', $target . '/../images/no-image-available.png');

        imagepng($img, Files::inc($target . '/test-image.jpg'));
    }
}
