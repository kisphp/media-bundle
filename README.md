# Usage Kisphp Media Bundle

Overwrite MediaFiles model 

```yaml
# app/config/services.yml
model.media_files:
  class: AppBundle\Model\ItemImageModel
  public: true
  arguments:
    - "@doctrine.orm.entity_manager"
```

Create AppBundle services:

```yaml
# src/AppBundle/Resources/config/services.yml
model.item:
    class: AppBundle\Model\ItemModel
    public: true
    arguments:
        - "@doctrine.orm.entity_manager"
        - "@knp_paginator"

media.model.factory:
  class: AppBundle\Services\MediaManagerFactory
  public: true
  arguments:
    - "@service_container"
```


Create a MediaManagerFactory service class

```php
<?php

namespace AppBundle\Services;

use Kisphp\MediaBundle\Services\AbstractMediaManagerFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MediaManagerFactory extends AbstractMediaManagerFactory
{
    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    protected function registerMediaModels(ContainerInterface $container)
    {
        $this->addMediaModels($container->get('model.item'));
    }
}
```

Create Uploaded image model:

```php
<?php

namespace AppBundle\Model;

use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\MediaBundle\Model\MediaFilesModel;

class ItemImageModel extends MediaFilesModel
{
    protected $objectType = ItemModel::MEDIA_TYPE_INVENTORY_ITEM;

    // optional method to modify entity before save
    public function preSaveActions(KisphpEntityInterface $entity)
    {
        $title = $entity->getTitle();

        if (preg_match('/\d+\.jpg/', $title)) {
            $max = strlen($title);

            $entity->setTitle(
                substr($title, max(0, $max-25), $max)
            );
        }
    }
}
```
