<?php

namespace Kisphp\MediaBundle\Services;

use Kisphp\MediaBundle\Model\MediaModelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractMediaManagerFactory
{
    /**
     * @var array
     */
    protected $mediaModels = [];

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->registerMediaModels($container);
    }

    /**
     * @return array
     */
    public function getMediaModels()
    {
        return $this->mediaModels;
    }

    /**
     * @param MediaModelInterface $mediaModels
     */
    public function addMediaModels(MediaModelInterface $mediaModels)
    {
        $this->mediaModels[$mediaModels->getMediaType()] = $mediaModels;
    }

    /**
     * @param string $type
     *
     * @return MediaModelInterface
     *
     * @throws \Exception
     */
    public function createMediaModelByType($type)
    {
        if (array_key_exists($type, $this->mediaModels) === false) {
            throw new \Exception('Media type not found');
        }

        return $this->mediaModels[$type];
    }

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    abstract protected function registerMediaModels(ContainerInterface $container);
}
