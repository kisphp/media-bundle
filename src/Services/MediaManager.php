<?php

namespace Kisphp\MediaBundle\Services;

use GuzzleHttp\Client;
use Kisphp\MediaBundle\Model\MediaFilesModel;
use Kisphp\MediaBundle\Model\MediaModelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MediaManager
{
    const IMG_WITH = 200;
    const IMG_HEIGHT = 200;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $pathName
     * @param array $arguments
     *
     * @return string
     */
    public function generateUrl($pathName, $arguments)
    {
        return $this->container
            ->get('router')
            ->generate($pathName, $arguments)
        ;
    }

    /**
     * @param Request $request
     * @param string $uploadType
     * @param int $id
     * @param array $metadata
     *
     * @return JsonResponse
     */
    public function uploadImages(Request $request, $uploadType, $id, array $metadata = [])
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');

        /** @var MediaFilesModel $imageModel */
        $imageModel = $this->get('model.media_files');
        /** @var MediaModelInterface $objectModel */
        $objectModel = $this->get('media.model.factory')->createMediaModelByType($uploadType);
        $object = $objectModel->find($id);

        /** @var \Kisphp\Utils\FilesManager $fileManager */
        $fileManager = $this->get('file_manager');

        /** @var \Kisphp\Entity\FileInterface|\Kisphp\Entity\KisphpEntityInterface|\Kisphp\MediaBundle\Entity\MediaFile $image */
        $image = $fileManager->uploadFile($file, $imageModel->createEntity());
        $image->setObjectType($objectModel->getMediaType());
        $image->setMetadata($metadata);
        $image->trimTitle();

        $image->setObject($object);

        // allow custom changes to the image entity before save
        $imageModel->preSaveActions($image);

        $imageModel->save($image);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'id' => $image->getId(),
            'url' => $this->generateUrl('thumbs', [
                'directory' => $image->getDirectory(),
                'filename' => $image->getFilename(),
                'width' => static::IMG_WITH,
                'height' => static::IMG_HEIGHT,
            ]),
            'title' => $image->getTitle(),
        ]);
    }

    /**
     * @param string $url
     * @param string $uploadType
     * @param int $objectId
     * @param array $metadata
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function downloadImageByUrl($url, $uploadType, $objectId, array $metadata = [])
    {
        $tmpFilePath = '/tmp/' . uniqid('media-', false);

        $this->curlImageFromUrl($url, $tmpFilePath);

        $up = new UploadedFile($tmpFilePath, basename($url), null, null, null, true);

        $request = Request::createFromGlobals();
        $request->files->add([
            'file' => $up,
        ]);

        return $this->uploadImages($request, $uploadType, $objectId, $metadata);
    }

    /**
     * @param string $sourceUrl
     * @param string $targetPath
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function curlImageFromUrl($sourceUrl, $targetPath)
    {
        $client = new Client();
        $client->request('GET', $sourceUrl, ['sink' => $targetPath]);
    }

    /**
     * @param string $serviceName
     *
     * @return object
     */
    protected function get($serviceName)
    {
        return $this->container->get($serviceName);
    }
}
