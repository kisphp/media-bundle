<?php

namespace Kisphp\MediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DropzoneController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $type
     * @param int $id
     *
     * @return JsonResponse
     */
    public function uploadAction(Request $request, $type, $id)
    {
        return $this->get('media_manager')->uploadImages($request, $type, $id);
    }
}
