<?php

namespace Kisphp\MediaBundle\Controller;

use Kisphp\ImageResizer;
use Kisphp\Utils\Files;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ThumbnailController extends Controller
{
    /**
     * @param Request $request
     * @param string $type
     *
     * @return JsonResponse
     */
    public function removeAction(Request $request, $type)
    {
        $id = $request->request->getInt('id');

        return $this->removeImage($id, $type);
    }

    /**
     * @param string $directory
     * @param int $width
     * @param int $height
     * @param string $filename
     *
     * @return Response
     */
    public function thumbAction($directory, $width, $height, $filename)
    {
        $rootDirectory = dirname($this->get('kernel')->getRootDir());
        $sourceDirectory = Files::inc($this->container->getParameter('upload_directory'));

        if (is_dir($sourceDirectory) === false) {
            mkdir($sourceDirectory, 0755, true);
        }

        $directoryTarget = sprintf(
            '%s/web/thumbs/%s',
            $rootDirectory,
            $directory
        );
        $fileTargetName = sprintf(
            '%dx%d_%s',
            $width,
            $height,
            $filename
        );

        if (is_dir($directoryTarget) === false) {
            mkdir($directoryTarget, 0755, true);
        }

        $sourceFile = Files::inc($sourceDirectory . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $filename);
        $targetFile = Files::inc($directoryTarget . DIRECTORY_SEPARATOR . $fileTargetName);

        if (is_file($sourceFile) === false) {
            $sourceFile = Files::inc($sourceDirectory . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'no-image-available.png');
        }

        $resizer = new ImageResizer();
        $resizer->load($sourceFile);
        $resizer->resize($width, $height, true);
        $resizer->setTarget($targetFile);

        $content = $resizer->display(true);

        return new Response($content);
    }

    /**
     * @param int $id
     * @param string $objectType
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function removeImage($id, $objectType)
    {
        $data = [
            'code' => 200,
            'id' => $id,
        ];

        $model = $this->get('model.media_files');

        $fileManager = $this->get('file_manager');

        $entity = $model->findOneBy([
            'id' => $id,
            'object_type' => $objectType,
        ]);

        if (empty($entity)) {
            return new JsonResponse([
                'code' => 404,
                'message' => 'Image not found in database',
            ]);
        }

        $fileManager->removeFile($entity);

        $model->remove($entity);

        return new JsonResponse($data);
    }
}
