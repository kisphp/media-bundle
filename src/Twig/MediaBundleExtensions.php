<?php

namespace Kisphp\MediaBundle\Twig;

use Kisphp\MediaBundle\Twig\Functions\ThumbFunction;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MediaBundleExtensions extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            $this->createThumbFunction(),
        ];
    }

    /**
     * @return \Twig_SimpleFunction
     */
    protected function createThumbFunction()
    {
        $router = $this->container->get('router');
        $thumb = new ThumbFunction($router);

        return $thumb->getExtension();
    }
}
