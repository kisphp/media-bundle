<?php

namespace Kisphp\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kisphp\Entity\FileInterface;
use Kisphp\Entity\KisphpEntityInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="media_files", options={"collate": "utf8_general_ci", "charset": "utf8"})
 */
class MediaFile implements FileInterface, KisphpEntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $id_object;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=24)
     */
    protected $object_type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=6, options={"fixed": true})
     */
    protected $directory;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $filename;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32)
     */
    protected $filetype;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, options={"default": ""})
     */
    protected $title = '';

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=255, options={"default": ""})
     */
    protected $metadata = '';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdObject()
    {
        return $this->id_object;
    }

    /**
     * @param int $id_object
     */
    public function setIdObject($id_object)
    {
        $this->id_object = $id_object;
    }

    /**
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     * @param string $object_type
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;
    }

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * @param string $directory
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        if ($this->filetype === null) {
            $this->setFiletype($this->calculateFileType($filename));
        }
    }

    /**
     * @return string
     */
    public function getFiletype()
    {
        return $this->filetype;
    }

    /**
     * @param string $filetype
     */
    public function setFiletype($filetype)
    {
        $this->filetype = $filetype;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function trimTitle()
    {
        $this->title = substr($this->title, 0, 250);
    }

    /**
     * @param \Kisphp\Entity\KisphpEntityInterface $object
     */
    public function setObject(KisphpEntityInterface $object)
    {
        $this->id_object = $object->getId();
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        if (empty($this->metadata)) {
            return [];
        }

        return json_decode($this->metadata, true);
    }

    /**
     * @param string $metadata
     */
    public function setMetadata(array $metadata)
    {
        $this->metadata = json_encode($metadata);
    }

    /**
     * @param string $filename
     *
     * @return string
     */
    protected function calculateFileType($filename)
    {
        $split = explode('.', $filename);

        return strtolower(end($split));
    }
}
