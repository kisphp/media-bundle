<?php

namespace Kisphp\MediaBundle\Model;

use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\MediaBundle\Entity\MediaFile;
use Kisphp\Model\AbstractModel;

class MediaFilesModel extends AbstractModel
{
    const REPOSITORY = 'MediaBundle:MediaFile';

    const OBJECT_TYPE_ARTICLE = 'article';
    const OBJECT_TYPE_CATEGORY = 'category';
    const OBJECT_TYPE_TEAM = 'team';
    const OBJECT_TYPE_WIDGET = 'widget';
    const OBJECT_TYPE_PRODUCT = 'product';
    const OBJECT_TYPE_PRODUCT_CATEGORY = 'product_category';

    /**
     * @var string
     */
    protected $objectType = '';

    /**
     * @param $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->objectType = $type;

        return $this;
    }

    /**
     * @param int $id
     *
     * @return null|object
     */
    public function find($id)
    {
        return $this->getRepository()
            ->findOneBy([
                'id' => $id,
                'object_type' => $this->objectType,
            ])
        ;
    }

    /**
     * @return MediaFile
     */
    public function createEntity()
    {
        return new MediaFile();
    }

    public function preSaveActions(KisphpEntityInterface $entity)
    {
        // use this method to make changes to the entity before save
    }
}
