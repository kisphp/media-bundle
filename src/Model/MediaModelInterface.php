<?php

namespace Kisphp\MediaBundle\Model;

interface MediaModelInterface
{
    /**
     * @return string
     */
    public function getMediaType();
}
